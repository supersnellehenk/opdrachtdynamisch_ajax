// AJAX

let brandArray = [];
let modelArray = [];
let versionArray = [];

function getBrands(brandDOM, modelDOM, versionDOM) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      brandArray = JSON.parse(this.responseText);
      createBrands(brandDOM, brandArray, modelDOM, versionDOM);
    }
  };
  xhttp.open("POST", "PHP/scripts/getCars.php", true);
  xhttp.send();
}

function createBrands(brandDOM, brandArray, modelDOM, versionDOM) {
  let brandOptions = `<option disabled selected>Selecteer een merk</option>`;
  brandArray.forEach(i => {
    brandOptions += `<option value="${i}">${i}</option>`;
  });
  brandDOM.innerHTML = brandOptions;
  modelDOM.innerHTML = `<option disabled selected>Selecteer een model</option>`;
  versionDOM.innerHTML = `<option disabled selected>Selecteer een uitvoering</option>`;
}

function getModels(brandValue, modelDOM, imageDOM, versionDOM) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      modelArray = JSON.parse(this.responseText);
      createModels(modelDOM, modelArray, imageDOM, versionDOM);
    }
  };
  xhttp.open("POST", "PHP/scripts/getCars.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(`brand=${brandValue}`);
}

function createModels(modelDOM, modelArray, imageDOM, versionDOM) {
  let modelOptions = `<option disabled selected>Selecteer een model</option>`;
  modelArray.forEach(i => {
    modelOptions += `<option value="${i}">${i}</option>`;
  });
  modelDOM.innerHTML = modelOptions;
  versionDOM.innerHTML = `<option disabled selected>Selecteer een uitvoering</option>`;
  imageDOM.innerHTML = '';
}

function getVersions(brandValue, modelValue, versionDOM, imageDOM) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      versionArray = JSON.parse(this.responseText);
      createVersions(versionDOM, versionArray, imageDOM);
    }
  };
  xhttp.open("POST", "PHP/scripts/getCars.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(`brand=${brandValue}&model=${modelValue}`);
}

function createVersions(versionDOM, versionArray, imageDOM) {
  let versionOptions = `<option disabled selected>Selecteer een uitvoering</option>`;
  versionArray.forEach(i => {
    versionOptions += `<option value="${i}">${i}</option>`;
  });
  versionDOM.innerHTML = versionOptions;
  imageDOM.innerHTML = '';
}

function getImage(brandValue, modelValue, versionValue, imageDOM) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      image = this.responseText;
      imageDOM.innerHTML = `<img src="${image}">`;
    }
  };
  xhttp.open("POST", "PHP/scripts/getCars.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(`brand=${brandValue}&model=${modelValue}&version=${versionValue}`);
}